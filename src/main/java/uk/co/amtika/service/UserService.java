package uk.co.amtika.service;

import uk.co.amtika.entity.User;

public interface UserService {
    public User findUserByUsername(String username);
    public void saveUser(User user);
}
