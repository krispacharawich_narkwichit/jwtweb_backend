package uk.co.amtika.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Splitter;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import uk.co.amtika.constant.AmtikaConstant;
import uk.co.amtika.entity.User;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import org.apache.commons.io.IOUtils;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {

        try {
            String body = IOUtils.toString(request.getReader());
            Map<String, String> userMap = Splitter.on("&").withKeyValueSeparator("=").split(body);
            Map<String, String> newMap = new HashMap<>();

            Iterator<Map.Entry<String, String>> iter = userMap.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<String, String> pair = iter.next();

                String key = "\"" + pair.getKey() + "\"";
                String value = "\"" + pair.getValue() + "\"";
                if (!pair.getKey().equalsIgnoreCase("submit"))
                    newMap.put(key, value);
            }

            User user = new ObjectMapper().readValue(newMap.toString().replaceAll("=", ":"), User.class);

            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    user.getUsername(),
                    user.getPassword(),
                    new ArrayList<>()
            ));

            ;
            return authentication;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain filterChain,
                                            Authentication authentication)
            throws IOException, ServletException {

        String token = Jwts.builder()
                .setSubject(((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername())
                .setExpiration(new Date(System.currentTimeMillis() + AmtikaConstant.HEADER_EXPIRED))
                .signWith(SignatureAlgorithm.HS512, AmtikaConstant.HEADER_SECRET.getBytes())
                .compact();
        response.addHeader(AmtikaConstant.HEADER_AUTHORIZATION, AmtikaConstant.HEADER_JWT_PREFIX + token);
        response.setStatus(HttpStatus.OK.value());
        response.setContentType("application/json");

    }

}
