package uk.co.amtika.security;

import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import uk.co.amtika.constant.AmtikaConstant;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain)
            throws IOException, ServletException {
        String headerAuthen = request.getHeader(AmtikaConstant.HEADER_AUTHORIZATION);
        if (validateHeader(headerAuthen) == false) {
            filterChain.doFilter(request, response);
            return;
        }

        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = getAuthenticationToken(request,
                headerAuthen);
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

        filterChain.doFilter(request, response);

    }

    private UsernamePasswordAuthenticationToken getAuthenticationToken(HttpServletRequest request, String header) {
        if (header != null) {
            String authenUser = Jwts.parser()
                    .setSigningKey(AmtikaConstant.HEADER_SECRET.getBytes())
                    .parseClaimsJws(header.replace(AmtikaConstant.HEADER_JWT_PREFIX, ""))
                    .getBody()
                    .getSubject();
            if (authenUser != null) {
                return new UsernamePasswordAuthenticationToken(authenUser,
                        null,
                        new ArrayList<>());
            }
            return null;
        }
        return null;
    }

    private boolean validateHeader(String header) {
        if ((header == null) || (!header.startsWith(AmtikaConstant.HEADER_JWT_PREFIX))) {
            return false;
        }
        return true;
    }
}
