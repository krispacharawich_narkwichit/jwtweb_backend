package uk.co.amtika.constant;

import org.springframework.stereotype.Component;

public class AmtikaConstant {

    public static final String DEFAULT_HOME_URL = "/";
    public static final String LOGIN_URL = "/login";
    public static final String REGISTER_URL = "/register";
    public static final String FAILURE_LOGIN_URL = "/login?error=true";
    public static final String DEFAULT_LOGIN_SUCCESS_URL = "/home";
    public static final String LOGOUT_URL ="/logout";
    public static final String ACCESS_DENIED_URL = "/access-denied";

    public static final String LOGIN_USERNAME = "username";
    public static final String LOGIN_PASSWORD = "password";

    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final long HEADER_EXPIRED = 1794000_000;
    public static final String HEADER_SECRET = "masterAmtikaSecretKey";
    public static final String HEADER_JWT_PREFIX = "Bearer ";
}
