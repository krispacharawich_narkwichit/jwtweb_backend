package JWTWebLogin;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import uk.co.amtika.Application;
import uk.co.amtika.entity.User;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
public class MainTest {

    public final String HOME_URL = "http://localhost:8080/";
    public final String LOGIN_URL = "http://localhost:8080/login";
    public final String REGISTER_URL = "http://localhost:8080/register";

    @Test
    public void testHome() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.exchange(HOME_URL, HttpMethod.GET, null ,String.class);
        System.out.println(response.getBody());
        if (response.getHeaders().containsKey("Authorization")) {
            System.out.println("Show JWT" + response.getHeaders().get("Authorization"));
        }
    }

    @Test
    public void testLogin() {
        RestTemplate restTemplate = new RestTemplate();

        User user = new User();
        user.setUsername("minimul");
        user.setPassword("sunderland123");

        HttpHeaders httpHeaders  = new HttpHeaders();
        httpHeaders.set("Content-Type","application/json");
        HttpEntity<User> httpEntity = new HttpEntity<>(user,httpHeaders);

        ResponseEntity<String> response = restTemplate.exchange(LOGIN_URL, HttpMethod.POST, httpEntity, String.class);
        System.out.println(response.getBody());
        if (response.getHeaders().containsKey("Authorization")) {
            System.out.println("Show JWT" + response.getHeaders().get("Authorization"));
        }
    }
  // admin,mypass | Wish, Coding123! | minimul, sunderland123
    @Test
    public void testRegister() {
        RestTemplate restTemplate = new RestTemplate();
        User user = new User();
        user.setUsername("minimul");
        user.setPassword("sunderland123");
        ResponseEntity<String> response = restTemplate.postForEntity(REGISTER_URL, user, String.class);
        System.out.println(response.getBody());
        if (response.getHeaders().containsKey("Authorization")) {
            System.out.println("Show JWT" + response.getHeaders().get("Authorization"));
        }
    }


}
