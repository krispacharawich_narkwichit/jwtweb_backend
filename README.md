JWTWeb_backend
    Simple Webapplication with signin/signup using JWT running by Spring.
    
Pre-requisites
1. JAVA 1.8

How to run the application.
1. mvn install -DskipTests
2. java -jar target/jwtweb_backend.jar